﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    public float initialSpeed;
    public float jumpForce;
    public int score;
    public Text uiText;
    public Text scoreText;

    private bool isDead = false;

    // Use this for initialization
    void Start () {
        GetComponent<Rigidbody2D>().velocity = Vector2.right * initialSpeed;

        uiText.text = "";
	}

    void Update ()
    {
        if (!isDead)
            score++;

        scoreText.text = "Score: " + score;

        if (Input.GetKeyDown("space"))
        {
            GetComponent<Rigidbody2D>().AddForce(Vector2.up * jumpForce);
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        GetComponent<Rigidbody2D>().isKinematic = true;
        GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        uiText.text = "Game Over :(";
        isDead = true;


        //GetComponent<Transform>().rotation = Quaternion.
        //Destroy(gameObject);
    }
}
